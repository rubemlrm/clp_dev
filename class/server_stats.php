<?php
	

class server_status{

	function format_time($seconds){
		$secs = intval($seconds % 60);
		$mins = intval($seconds / 60 %60);
		$hours = intval($seconds /3600 % 24);
		$days = intval($seconds /86400);
     		if($days < 1){
     			$uptimeString =$hours."h:".$mins."m:".$secs."s";
   			 }else{
    			$uptimeString = $days ."d:".$hours."h:".$mins."m:".$seconds."s";
   			}
	   	return $uptimeString;
	}
	function verify_load($load, $numberOfCores){
			$loadsize = ($load / 10) * 100;
			if($numberOfCores == 1){
			if($load < 0.70){
				$loadbar = '<div class="load-bar" style="width:'.$loadsize.'px;background-color: #86e01e"><span id="avg">'.$load.'</span></div>';
				
			}elseif ($load > 0.70 && $load < 5.0) {
				$loadbar = '<div class="load-bar" style="width:'.$loadsize.'px;background-color: #f2b01e"><span id="avg">'.$load.'</span></div>';
			}else{
				$loadbar = '<div class="load-bar" style="width:'.$loadsize.'px;background-color: #f63a0f"><span id="avg">'.$load.'</span></div>';
			}

		}else{
			if($load < 1.70){
				$loadbar = '<div class="load-bar" style="width:'.$loadsize.'px;background-color: #86e01e"><span id="avg">'.$load.'</span></div>';
				
			}elseif ($load > 1.70 && $load < $numberOfCores) {
				$loadbar = '<div class="load-bar" style="width:'.$loadsize.'px;background-color: #f2b01e"><span id="avg">'.$load.'</span></div>';
			}else{
				$loadbar = '<div class="load-bar" style="width:'.$loadsize.'px;background-color: #f63a0f"><span id="avg">'.$load.'</span></div>';
			}
		}
		return $loadbar;
	}
}