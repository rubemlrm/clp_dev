<?php

class project_management{
	
	function list_dir($dir){
		$dh = opendir("$dir");
		while($file = readdir($dh)){
			if($file != "." && $file != ".."){
				$list[] = $file;
			}
		}	
		return $list;
	}


    function list_zip($dir){
        $dh = opendir("$dir");
        while($file = readdir($dh)){
            if($file != "." && $file != ".."){
                if(!is_dir($file)){
                $list[] = $dir."/".$file;
            }else{
                $file= $dir."/".$file;  
                $tempdir = $this->list_zip($file);
                $list = array_merge($list, $tempdir);
            }
        } 
        }
        return $list;
    }
	function emptyDirectory($dirname,$self_delete=false) {
        $dir_handle = "";
       if (is_dir($dirname)){
          $dir_handle = opendir($dirname);
          }
       if (!$dir_handle){
          return false;
          }
       while($file = readdir($dir_handle)) {
          if ($file != "." && $file != "..") {
             if (!is_dir($dirname."/".$file))
                chmod($dirname."/".$file, 0770);
                @unlink($dirname."/".$file);
             }else{
                emptyDirectory($dirname.'/'.$file,true);    
          }
       }
       closedir($dir_handle);
       if ($self_delete){
            @rmdir($dirname);
       }   
       return true;
}

    //function created by david walsh
    function create_zip($files = array(),$destination="backups/",$overwrite = false) {
      //if the zip file already exists and overwrite is false, return false
      if(file_exists($destination) && !$overwrite) { return false; }
      //vars
      $valid_files = array();
      //if files were passed in...
      if(is_array($files)) {
        //cycle through each file
        foreach($files as $file) {
          //make sure the file exists
          if(file_exists($file)) {
            $valid_files[] = $file;
          }
        }
      }
      //if we have good files...
      if(count($valid_files)) {
        //create the archive
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
          return false;
        }
        //add the files
        foreach($valid_files as $file) {
          $zip->addFile($file,$file);
        }
        //debug
        #echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
        
        //close the zip -- done!
        $zip->close();
        
        //check to make sure the file exists
        return file_exists($destination);
      }
      else
      {
        return false;
      }
}
        function download_zip($file){
            if(!file_exists($file)){
                die("ERROR");
            }else{
                 header("Cache-Control: public");
                 header("Content-Description: File Transfer");
                 header("Content-Disposition: attachment; filename=$file");
                 header("Content-Type: application/zip");
                header("Content-Transfer-Encoding: binary");
                readfile($file);
                unlink($file);
            }
        }
}
