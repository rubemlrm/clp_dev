<?
require './class/database.php';
$database = new database();
?>

<!DOCTYPE HTML>
<html lang="pt-PT">
<head>
	<meta charset="UTF-8">
	<title>Localhost Control Panel</title>
  <link rel="stylesheet" href="css/style.css">
  <script src="http://code.jquery.com/jquery-latest.js"></script>
  <script src="js/modernizr.js"></script>
 <!--[iflt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

 <script>
			(function($){
				
				//cache nav
				var nav = $("#topNav");
				
				//add indicator and hovers to submenu parents
				nav.find("li").each(function() {
					if ($(this).find("ul").length > 0) {
						$("<span>").text("^").appendTo($(this).children(":first"));

						//show subnav on hover
						$(this).mouseenter(function() {
							$(this).find("ul").stop(true, true).slideDown();
						});
						
						//hide submenus on exit
						$(this).mouseleave(function() {
							$(this).find("ul").stop(true, true).slideUp();
						});
					}
				});
			})(jQuery);
		</script>
</head>
<body>
	<header>
		<h1 id="logo">Localhost Control Panel</h1>
	<nav id="topNav">
		<ul>
				<li><a href="index.php">Home Page</a></li>
				<li><a href="projectos.php">Projectos</a></li>
				<li class="dropdown"><a href="#">Documentação</a>
					<ul class="large">
						<li><a href="http://www.php.net/manual/en/">PHP</a></li>
						<li><a href="http://dev.mysql.com/doc/">MySql</a></li>
						<li><a href="http://httpd.apache.org/docs/2.4/">Apache</a></li>
					</ul>
				<li><a href="about.php">Sobre </a></li>
		</ul>
	</nav>
	</header>
	<div class="left_bar">
		
		<? require 'includes/left_sidebar.php';
		?>
	</div>
	<div class="container">